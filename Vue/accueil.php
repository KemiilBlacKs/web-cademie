<?php include ('../Back/tweets.php');
include ('../Back/hashtag.php');
include ('../Back/@.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="CSS/acceuil.css">
	<link href='http://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="jQuery/tweet.js" type="text/javascript" charset="utf-8" async defer></script>
	<meta charset="UTF-8">
</head>
<body>
	<header class="container-fluid">
		<h3><span class="blue"><a href='accueil.php'><img src="Misc/logo.png" alt="logo" />ACCEUIL</a></span></h3>
		<!-- <div class="row"> -->
	        <div class="col-md-4 col-md-offset-3">
	            <form action="" class="search-form">
	                <div class="form-group has-feedback">
	            		<label for="search" class="sr-only">Search</label>
	            		<input type="text" class="form-control" name="search" id="search" placeholder="Recherche...">
	              		<span class="glyphicon glyphicon-search form-control-feedback"></span>
	            	</div>
	            </form>
	        </div>
    	<!-- </div> -->
		<img src="../Vue/Misc/avatar.png"  class="img-thumbnail" alt="avatar" width="70">
		<p><?php echo $_SESSION['pseudo'];?></p>
		<div class="deconnexion"><button type="button" class="btn btn-danger btn-lg btn3d"><span class="glyphicon glyphicon-off"></span></button></div>
	</header>
	<div class="clear"></div>

</div>
<div class="container-fluid col-md-12">
	<div class="profil col-md-2 col-md-offset-1">
		<img src="Misc/avatar.png" class="img-circle" alt="avatar" width="200">
		<p><span class="pseudo"><?php echo '@'.$_SESSION['pseudo'];?></span></p>
		<div class="infos-profil">
			<div class="nbr-tweet">
				<span><?php $user = new Listing();
$user->countTweet();
?></span><br>
					<p class="type-tweet">TWEETS</p>
				</div>
			</div>
		</div>
		<div class="container-centre col-md-6">
			<link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
<div class="container">
    <div class="row">

    <div class="col-md-6">
    						<div class="widget-area no-padding blank">
								<div class="status-upload">
									<form>
										<textarea placeholder="Quoi de neuf ?" ></textarea>
										<ul>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i class="fa fa-music"></i></a></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i class="fa fa-video-camera"></i></a></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Sound Record"><i class="fa fa-microphone"></i></a></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i></a></li>
										</ul>
										<button type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Tweeter</button>
									</form>
<?php
if (!empty($_POST['search'])) {
	if (strstr($_POST['search'], '#')) {
		$pos  = strpos($_POST['search'], '#');
		$rest = substr($_POST['search'], ($pos+1));
		$user = new Hash();
		$user->listingHashtagFilter($rest);
	}
	if (strstr($_POST['search'], '@')) {
		$pos  = strpos($_POST['search'], '@');
		$rest = substr($_POST['search'], ($pos+1));
		$user = new Search($rest);
		$user->search($rest);
	}
} else {
	$user = new Listing();
	$user->listing();
}
?>
								</div>
							</div>
						</div>

    </div>
</div>
		</div>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="jQuery/app.js" type="text/javascript" charset="utf-8" async defer></script>
	</body>
</html>