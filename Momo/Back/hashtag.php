<?php
class Hash{
	public $bdd;

	public function __construct(){
		$this->bdd = new PDO("mysql:host=localhost;dbname=mytweeter","root","jejoja19");
		$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	}
	public function insertHashtag($hash, $id){
		$request = $request->bdd->prepare("INSERT INTO tags(tag,id_tweet) VALUES(':value',':id')");
		$request->bindParam(':value',$hash);
		$request->bindParam(':id',$id);
		$request->execute();
	}
	public function listingHashtagFilter($value){
		$request = $this->bdd->prepare("SELECT content,date,members.pseudo FROM tweets INNER JOIN members on tweets.id_member = members.id INNER JOIN tags on tags.id_tweet = tweets.id_tweet WHERE tags.tag LIKE ':tag'");
		$request->bindParam(':tag',$value);
		$request->execute();
		while($data = $request->fetch()){
			if ($data === NULL) {
				echo "Aucun tweets comportant ce hashtag n'a été trouvé !";
				return true;
			}
			else{
	 			echo "<div id='tweazee'><p id='date'>".$data[1]."<br /></p><p><span>".$_SESSION['pseudo']."</span></p><br /><p id='content'>".$data[0]."</p></div>";
			}
		}
	}
	public function listHashtag(){
		$request = $this->bdd->prepare("SELECT tag FROM tags INNER JOIN tweets on tags.id_tweet = tweets.id ORDER BY tweets.date desc  LIMIT 5");
		$request->execute();
		while($data = $request->fetch()){
			echo $data[0];
		}
	}
}
?>