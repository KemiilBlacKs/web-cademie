
<?php
include ('../Back/inscription.php');
// include ('verif.php');

session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="CSS/inscription.css">
	<meta charset="UTF-8">
</head>
<body>
<ul>
<li><a href='accueil.php'>Acceuil</a></li>
<li><a href='inscription.php'>Inscription</a></li>
<li><a href='connexion.php'>Connexion</a></li>
</ul>
<div class="container">
      <form class="form-signin" method="post" action="inscription.php">
        <h2 class="form-signin-heading">Comment s'inscrire ?  </h2>
        <h4>Partie utilisateur</h4>
        <input type='text' name='pseudo' class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name='password' id="inputPassword" class="form-control" placeholder="Password" required>
        <input type="password" name='confirm' class="form-control" placeholder="Confirm your password" required>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name='email' id="inputEmail" class="form-control" placeholder="Email address" required>
		<h4>Partie personnelle de l'utilisateur</h4>
        <input type='text' name='nom' class="form-control" placeholder="Your name" required>
        <input type="text" name='ville' class="form-control" placeholder="City" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">S'inscrire</button>
      </form>

    </div>
<?php
if (isset($_POST['pseudo']) && isset($_POST['password']) && isset($_POST['confirm']) && isset($_POST['email']) && isset($_POST['nom']) && isset($_POST['ville'])) {
    $_SESSION['pseudo'] = $_POST['pseudo'];
    $_SESSION['password'] = $_POST['password'];
	$user = new Inscription($_POST['pseudo'], $_POST['password'], $_POST['nom'], $_POST['email'], $_POST['ville']);
	$user->enregistrer($_POST['pseudo'], $_POST['password'], $_POST['nom'], $_POST['email'], $_POST['ville']);
	echo "Bravo vous etes inscrit !\n";
	echo 'Veuillez contactez un Administrateur pour vous activer votre compte';
}
else {
	echo 'Veuillez remplir correctement tout les champs !';
}
?>
</body>
</html>