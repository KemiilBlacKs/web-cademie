<?php session_start();
include('../Back/membre.php');  ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="CSS/membre.css">
  <link href='http://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <header class="container-fluid">
   <h3><span class="blue"><a href='accueil.php'><img src="Misc/logo.png" alt="logo" />ACCEUIL</a></span></h3>
   <input type="search" class="form-control" placeholder="Recherche..." name='search'>
   <input type="submit" class="btn btn-primary" value="OK">
   <img src="../Vue/Misc/avatar.png"  class="img-thumbnail" alt="avatar" width="70">
   <p><?php echo $_SESSION['pseudo']; ?></p>
 </header><!-- /header -->

 <div class="container">
  <h1>Modification du profil</h1>
  <hr>
  <div class="row">
    <!-- left column -->
    <div class="col-md-3">
      <div class="text-center">
        <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
        <h6>Télécharger une autre photo...</h6>

        <input type="file" class="form-control">
      </div>
    </div>

    <!-- edit form column -->
    <div class="col-md-9 personal-info">
      <div class="alert alert-info alert-dismissable">
        <a class="panel-close close" data-dismiss="alert">×</a>
        <i class="fa fa-coffee"></i>
        Pensez à <strong>bien remplir</strong> les champs puis à sauvegarder vos modifications
      </div>
      <h3>Infos Personnelles</h3>

      <form class="form-horizontal" role="form" method="post">
        <div class="form-group">
          <label class="col-lg-3 control-label">Nom:</label>
          <div class="col-lg-8">
            <input class="form-control" type="text" placeholder="Your name" name='nom'>
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-3 control-label">Email:</label>
          <div class="col-lg-8">
            <input class="form-control" type="text" value="<?php echo $_SESSION['email']; ?>" name='mail'>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Pseudo:</label>
          <div class="col-md-8">
            <input class="form-control" type="text" value="<?php echo $_SESSION['pseudo']; ?>" name='pseudo'>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Mot de passe:</label>
          <div class="col-md-8">
            <input class="form-control" type="password" value="<?php echo $_SESSION['password']; ?>" name='password'>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Confirmation Mot de passe:</label>
          <div class="col-md-8">
            <input class="form-control" type="password" value="<?php echo $_SESSION['password']; ?>" name='confirm'>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label"></label>
          <div class="col-md-8">
            <input type="submit" class="btn btn-primary" value="Sauvegarder">
            <span></span>
            <input type="reset" class="btn btn-default" value="Annuler">
          </div>
        </div>
      </form>
      <?php
      if (!empty($_POST['search'])) {
        $user = new Member();
        $user->listMembersFilter($_POST['search']);
      }
      if (isset($_POST['nom']) && isset($_POST['pseudo']) &&
        isset($_POST['mail']) && isset($_POST['password']) && 
        isset($_POST['confirm'])){
        echo 'lol';
      $user = new Member($_POST['nom'],$_POST['mail'],$_POST['pseudo'],
        $_POST['password'],$_POST['confirm']);
      $user->verif($_POST['nom'],$_POST['mail'],$_POST['pseudo'],
        $_POST['password'],$_POST['confirm']);
    }

    ?>
  </div>
</div>
</div>
</body>
</html>