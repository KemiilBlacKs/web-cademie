<?php
session_start();

?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="CSS/connect.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Connexion à Twitter Académie !</title>
  </head>
  <body>
    <ul>
        <li><a href='accueil.php'><span>Acceuil</span></a></li>
        <li><a href='inscription.php'>Inscription</a></li><!-- 
        <li><a href='connexion.php'>Connexion</a></li>
    </ul> -->

    <div class="container">

      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Comment se connecter ?  </h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password"  id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter </button>
      </form>
<?php
if (isset($_POST['email']) && isset($_POST['password'])) {
	include ('../Back/connexion.php');
	$user = new Connect($_POST['email'], $_POST['password']);
	$user->connect($_POST['email'], $_POST['password']);

}
?>
</div>
  </body>
</html>
