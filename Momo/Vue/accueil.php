 <?php include('../Back/tweets.php'); 
  include('../Back/hashtag.php'); 
  include('../Back/@.php');
  ?>
<!DOCTYPE html>
<html>
<head>
	<title>Accueil</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="CSS/acceuil.css">
	<link href='http://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>
	<meta charset="UTF-8">
</head>
<body>
<header class="container-fluid">
	<h3><span class="blue"><a href='accueil.php'><img src="Misc/logo.png" alt="logo" />ACCEUIL</a></span></h3>
		<form action="accueil.php" method="post" accept-charset="utf-8">
			<input type="search" class="form-control" placeholder="Recherche..." name='search'>
			<input type="submit" class="btn btn-primary" value="OK">
		</form>
	<a href='membre.php'><img src="../Vue/Misc/default-avatar.jpg" class="img-thumbnail" alt="avatar" width="70"></a>
	<p><?php echo $_SESSION['pseudo']; ?></p>
</header>
<div class="clear"></div>
<div class="container-fluid col-md-12">
	<div class="profil col-md-2 col-md-offset-1">
		 <img src="Misc/avatar.png" class="img-circle" alt="avatar" width="200">
		<p><span class="pseudo"><?php echo '@'.$_SESSION['pseudo']; ?></span></p>
		<div class="infos-profil">
			<div class="nbr-tweet">
				<span><?php $user = new Listing(); $user->countTweet(); ?></span><br>
				<p class="type-tweet">TWEETS</p>
 			</div> 
		</div>
		</div>
	<div class="container-centre col-md-6">
		<div class="input-group">
		<form action="accueil.php" method="post" accept-charset="utf-8">			
		    <textarea id="write" rows="3"  placeholder='Etalez votre vie ICI !' name='tweet'></textarea>
		    <input type='submit' value='Tweeter!'/>
		</form>
		<div id='list'>
		<?php
				if (!empty($_POST['search'])) {
					if (strstr($_POST['search'], '#')) {
						$pos = strpos($_POST['search'], '#');
			 			$rest = substr($_POST['search'], ($pos+1));
			 			$user = new Hash();
			 			$user->listingHashtagFilter($rest);
					}
					if (strstr($_POST['search'], '@')) {
						$pos = strpos($_POST['search'], '@');
						$rest = substr($_POST['search'], ($pos+1));
						$user = new Search($rest);
						$user->search($rest);
					}
				}
				elseif(!empty($_POST['tweet'])){
					if (strlen($_POST['tweet']) < 140){
						$user = new Listing();
						$user->insertTweet($_POST['tweet']);
						$user = new Listing();
			 			$user->listing();
					}
					else {
						$utilisateur = new Listing();
						$utilisateur->listing();
					}
				}
				else{
					$user = new Listing();
			 		$user->listing();
			 	}
		    ?>
		</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="jQuery/app.js" type="text/javascript" charset="utf-8" async defer></script>
</body>
</html>