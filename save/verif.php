<?php
class Verif {
	public $bdd;
public function __construct($bdd) {
	$this->bdd = new PDO("mysql:unix_socket=/home/dall-o_j/.mysql/mysql.sock;port=port;dbname=mytweeter", "root" ,"");
	$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
public function verifPseudo($pseudo) {

	$pseudo = htmlspecialchars($pseudo);
	$request = $this->bdd->prepare("SELECT EXISTS(SELECT pseudo from members WHERE pseudo like ':pseudo')");
	$request->bindParam(':pseudo',$pseudo);
	$request->execute();
	$data = $request->fetch();
	if ($data === 1) {
		echo "Ce pseudo existe déjà !\n";
	}
	else {
			return true;
		}
}
public function verifEmail($email) {
	$email = htmlspecialchars($email);
	$request = $this->bdd->prepare("SELECT EXISTS(SELECT mail from members WHERE email like ':email')");
	$request->bindParam(':email',$email);
	$request->execute();
	$data = $request->fetch();
		if ($data === 1) {
			echo "Cet email existe déjà !\n";
		}
		else {
			return true;
		}
	}
public function verifName($nom) {
	$nom = htmlspecialchars($nom);
	$request = $this->bdd->prepare("SELECT EXISTS(SELECT name from members WHERE name like 'nom')");
	$request->bindParam(':nom',$nom);
	$request->execute();
	$data = $request->fetch();
		if ($data === 1) {
			echo "Ces identifiants sont déjà utilisés !\n";
		}
		else {
			return true;
		}
	}
}
?>