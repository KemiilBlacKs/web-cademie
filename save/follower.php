<?php
class Follow {

public $bdd;

	public function __construct($bdd) {
		$this->bdd = new PDO("mysql:host=localhost;dbname=mytweeter","root","");
		$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	}
	public function listFollowers($leader) {
		$request = $this->bdd->prepare("SELECT members.pseudo FROM members INNER JOIN followers ON followers.id_member = members.id WHERE members.id = followers.id_follower AND follower.id_member = ':id'")
		$request->bindParam(':id',$leader);
		$request->execute();
		$data = $request->fetch();
		echo $data;
	}
	public function countFollowers($leader) {
		$request = $this->bdd->prepare("SELECT count(members.pseudo) FROM members INNER JOIN followers ON followers.id_member = members.id WHERE members.id = followers.id_follower AND follower.id_member = ':id'")
		$request->bindParam(':id',$leader);
		$request->execute();
		$data = $request->fetch();
		echo $data;
	}
				// PAS FONCTIONNELLES
	// public function listFollowings($pseudo) {
		// 	$request = $this->bdd->prepare("SELECT members.pseudo FROM members INNER JOIN followers ON members.id = followers.id_member WHERE followers.id_member = members.id");
		// 	$request->bindParam(':pseudo',$pseudo);
		// 	$request->execute();
	//  }
	// public function countFollowings($pseudo) {
		// 	$request = $this->bdd->prepare("SELECT count(members.pseudo) FROM members INNER JOIN followers ON members.id = followers.id_member WHERE followers.id_member = members.id");
		// 	$request->bindParam("");
		// 	$request->execute();
		// 	$data = $request->fetch();
		// 	echo $data;
	// }

}
?>