<?php
class RT {
public $bdd;

	public function __construct($bdd) {
		$this->bdd = new PDO("mysql:host=localhost;dbname=mytweeter","root","");
		$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	}
	public function insertRT($id_member,$id_tweet) {
		$request = $this->bdd->prepare("INSERT INTO retweets(date,id_member,id_tweet) VALUES(curdate(),':id',':id_tweet')");
		$request->bindParam(':id',$id_member);
		$request->bindParam(':id_tweet',$id_tweet);
		$request->execute();
	}
	public function selectRT($id_tweet) {
		$request = $this->bdd->prepare("SELECT content,date,members.pseudo FROM tweets INNER JOIN members on members.id = tweets.id_member INNER JOIN retweets on retweets.id_tweet = tweets.id_tweet WHERE id_tweet = ':id_tweet')");
		$request->bindParam(':id_tweet',$id_tweet);
		$request->execute();
		$data = $request->fetch();
	}
}
?>
