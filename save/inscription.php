<?php
class Inscription {
	public $bdd;

		public function __construct($bdd) {
		$this->bdd = new PDO("mysql:host=localhost;dbname=mytweeter","root","");
		$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	}
		public function enregistrer($pseudo,$password,$name,$email,$location) {
			// $password = hash('ripemd160', $password);
			$request = $this->bdd->prepare("INSERT INTO members(pseudo,password,name,mail,location) VALUES(:pseudo,:password,:nom,:email,:location)");
			$request->bindParam(':pseudo',$pseudo);
			$request->bindParam(':password',$password);
			$request->bindParam(':nom',$name);
			$request->bindParam(':email',$email);
			$request->bindParam(':location',$location);
			$request->execute();
			return true;
		}
}
?>